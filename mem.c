#include "mem.h"
#include "common.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

// constante définie dans gcc seulement
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

struct free_block {
	size_t size;
	struct free_block* next;
};

struct free_block* first_free_block = NULL;

struct used_block {
	size_t size;
	struct used_block* next;
};

struct used_block* first_used_block = NULL;

void mem_init(void* mem, size_t taille)
{
	assert(mem == get_memory_adr());
	assert(taille == get_memory_size());

	struct free_block* zone_libre = (struct free_block*)mem;
	zone_libre->size = taille;						//init la premiere zone libre
	zone_libre->next = NULL;

	first_free_block = zone_libre;
	mem_fit(&mem_fit_first);
}

void mem_show(void (*print)(void *, size_t, int)) {
	struct free_block* free_block_current = first_free_block;
	struct used_block* used_block_current = first_used_block;

	while(free_block_current != NULL || used_block_current != NULL){
		if(used_block_current == NULL || (free_block_current != NULL && (free_block_current < (struct free_block*) used_block_current))){
			print(free_block_current,free_block_current->size,1);
			free_block_current = free_block_current->next;
		}
		else {
			print(used_block_current,used_block_current->size,0);
			used_block_current = used_block_current->next;
		}
	}
}



static mem_fit_function_t *mem_fit_fn;

void mem_fit(mem_fit_function_t *f) {
	mem_fit_fn=f;
}



void *mem_alloc(size_t taille) {
	__attribute__((unused))
	size_t true_size = taille + sizeof(struct used_block);
	true_size =(((true_size)+(ALIGNMENT)-1) & ~(ALIGNMENT-1));				//on aligne la taille

	struct free_block* zone_found = mem_fit_fn(first_free_block,true_size);
	if (zone_found == NULL){
		return NULL;;
	}

	struct used_block* new_used_zone= (struct used_block*) zone_found;
	new_used_zone ->next = NULL;
	struct free_block* new_free_block = NULL;										//au cas ou il reste de la place dans la zone libre

	struct free_block* parent_free_block = first_free_block;
	struct used_block* parent_used_block = first_used_block;


	if(true_size >= zone_found->size - sizeof(struct free_block)){
		new_used_zone->size = zone_found->size;
	}else{
		new_free_block = ((struct free_block*) ((char*)zone_found + true_size));
		new_free_block ->size = zone_found->size - true_size;
		new_free_block->next = NULL;
		new_used_zone->size = true_size;
	}

	if(first_used_block == NULL){										//Gestion des zones occupés
		first_used_block = new_used_zone;
		new_used_zone->next = NULL;
	}else if(first_used_block > new_used_zone){
		new_used_zone->next=first_used_block ;
		first_used_block = new_used_zone;
	}else{
		while(parent_used_block->next != NULL && parent_used_block->next < new_used_zone){
			parent_used_block = parent_used_block->next;
		}
		new_used_zone->next = parent_used_block->next;
		parent_used_block->next = new_used_zone;
	}

	if(first_free_block == zone_found){										//Gestion des zones libres
		if(new_free_block != NULL){
			new_free_block->next = first_free_block->next;
			first_free_block = new_free_block;
		}else{
			first_free_block = first_free_block->next;
		}
	}else{
		while(parent_free_block->next != zone_found){
			parent_free_block = parent_free_block->next;
		}
		if(new_free_block != NULL){
			new_free_block->next = zone_found->next;
			parent_free_block->next = new_free_block;
		}else{
			parent_free_block->next = zone_found->next;
		}
	}

	return ((void*)((char*)new_used_zone + sizeof(struct used_block)));
}




void mem_free(void* mem) {
	struct free_block* new_free_block = NULL;
	void* mem_to_free = ((void*)((char*)mem - sizeof(struct used_block)));

	if((void*)first_used_block == mem_to_free){						//on libere la memoire
		new_free_block = ((struct free_block*)mem_to_free);
		new_free_block->size = first_used_block->size;
		first_used_block = first_used_block->next;
	}else{
		struct used_block* parent_used_block = first_used_block;
		while(parent_used_block != NULL && parent_used_block->next != (struct used_block*)mem_to_free){
			parent_used_block = parent_used_block->next;
		}
		if(parent_used_block == NULL){
			//on a une erreur;
			return;
		}
		new_free_block = ((struct free_block*)parent_used_block->next);
		new_free_block->size = parent_used_block->next->size;
		parent_used_block->next =parent_used_block->next->next;
	}

	new_free_block->next = NULL;								//on va replacer la memoire libéré au bon endroit
	if(first_free_block == NULL){
		first_free_block = new_free_block;
	}else if(first_free_block > new_free_block ){
		new_free_block->next = first_free_block;
		first_free_block = new_free_block;

		if(new_free_block->next == ((struct free_block*)((char*)new_free_block+new_free_block->size))){
			new_free_block->size = new_free_block->size + new_free_block->next->size;
			new_free_block->next = new_free_block->next->next;
		}
	}else{
		struct free_block* parent_free_block = first_free_block;
		while(parent_free_block->next != NULL && parent_free_block->next < new_free_block ){
			parent_free_block = parent_free_block->next;
		}
		new_free_block->next = parent_free_block->next;
		parent_free_block->next = new_free_block;

		if(parent_free_block->next->next == ((struct free_block*)((char*)new_free_block+new_free_block->size))){		//on fusionne avec la zone libre suivante
			new_free_block->size = new_free_block->size + parent_free_block->next->next->size;
			new_free_block->next = new_free_block->next->next;
		}

		if(((struct free_block*)((char*)parent_free_block+parent_free_block->size)) == new_free_block){							//on fusionne avec la zone libre precedente
			parent_free_block->size= parent_free_block->size + new_free_block->size;
			parent_free_block->next = new_free_block->next;
		}
	}
}



struct free_block* mem_fit_first(struct free_block *list, size_t size) {
	if(list != NULL && first_free_block !=NULL){
		struct free_block* current = first_free_block;
		while(current != NULL){
			if(size <= current->size){
				return current;
			}
			current = current->next;
		}
	}
	return NULL;
}


/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stused_block.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stused_block.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	return 0;
}



struct free_block* mem_fit_best(struct free_block *list, size_t size) {
	//ToDo
	return NULL;
}

struct free_block* mem_fit_worst(struct free_block *list, size_t size) {
	//ToDo
	return NULL;
}
